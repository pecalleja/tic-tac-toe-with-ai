from collections import Counter
from abc import ABC, abstractmethod
from random import choice


class InvalidString(ValueError):
    def __init__(self):
        super().__init__("Invalid initialization string")


class TicTacToeBoard:
    matrix: list
    x_len = 3
    y_len = 3

    def __init__(self, symbols: str):
        self.symbols = symbols
        self.matrix = [[" " for _y in range(self.y_len)] for _x in range(self.x_len)]
        if len(symbols) != self.x_len * self.y_len:
            raise InvalidString
        for index, symbol in enumerate(symbols):
            if symbol not in ("X", "O", "_"):
                raise InvalidString
            if symbol in ("X", "O"):
                y_index = index // self.y_len
                x_index = index % self.x_len
                self.matrix[y_index][x_index] = symbol

    def test_winner_move(self, symbol, y, x):
        temporal = self.matrix[y][x]
        self.matrix[y][x] = symbol
        if self.game_finished() == symbol:
            result = True
        else:
            result = False
        self.matrix[y][x] = temporal
        return result

    def game_finished(self):
        all_rows = ["".join(x) for x in self.matrix]
        traverse = [[self.matrix[j][i] for j in range(self.y_len)] for i in range(self.x_len)]
        all_coll = ["".join(x) for x in traverse]
        diagonal_down = "".join([all_rows[0][0], all_rows[1][1], all_rows[2][2]])
        diagonal_up = "".join([all_rows[2][0], all_rows[1][1], all_rows[0][2]])
        all_combination = all_rows + all_coll + [diagonal_down, diagonal_up]
        if "XXX" in all_combination:
            return "X"
        elif "OOO" in all_combination:
            return "O"
        for item in all_rows:
            if " " in item:
                return False
        return True

    def _place_holder(self):
        total = self.y_len * self.x_len
        return len(str(total))

    def _border_length(self, placeholder):
        return self.x_len * (placeholder + 1) + 3

    def render(self):
        placeholder = self._place_holder()
        border_length = self._border_length(placeholder)
        border = "-"*border_length
        print(border)
        for i, all_row in enumerate(self.matrix):
            row = "|"
            for j, element in enumerate(all_row):
                row += " " + element.rjust(placeholder, element)
            row += " |"
            print(row)
        print(border)

    def get_empty_cells(self):
        empty_cells = []
        for y, row in enumerate(self.matrix):
            for x, cell in enumerate(row):
                if cell == " ":
                    empty_cells.append((y, x))
        return empty_cells

    def make_move(self, symbol, y, x):
        self.matrix[y][x] = symbol


class Player(ABC):
    symbol: str
    opponents = {
        "X": "O",
        "O": "X"
    }

    def __init__(self, symbol):
        self.symbol = symbol

    @abstractmethod
    def get_move(self, board: TicTacToeBoard) -> tuple:
        raise NotImplementedError

    @staticmethod
    def valid_input_values(values) -> tuple:
        try:
            if len(values) != 2:
                raise ValueError
            y = int(values[0])
            x = int(values[1])
            if not (x or y):
                raise ValueError
        except ValueError as e:
            print("You should enter numbers!")
            raise e
        return y, x

    @property
    def opponent(self):
        return self.opponents.get(self.symbol)


class ComputerPlayer(Player):
    level = "easy"

    def making_move(self):
        print(f'Making move level "{self.level}"')

    def get_move(self, board) -> tuple:
        self.making_move()
        return choice(board.get_empty_cells())


class MediumComputerPlayer(ComputerPlayer):
    level = "medium"

    def get_move(self, board) -> tuple:
        blank_options = board.get_empty_cells()
        self.making_move()
        for option in blank_options:
            if board.test_winner_move(self.symbol, *option):
                return option
        for option in blank_options:
            if board.test_winner_move(self.opponent, *option):
                return option
        return choice(blank_options)


class HumanPlayer(Player):
    def get_move(self, board: TicTacToeBoard) -> tuple:
        x, y = False, False
        while not all([x, y]):
            text = input("Enter the coordinates:")
            try:
                if not text:
                    print("You should enter numbers!")
                    raise ValueError
                result = text.split()
                y, x = self.valid_input_values(result)
                if not (1 <= x <= 3 and 1 <= y <= 3) or not (x or y):
                    print("Coordinates should be from 1 to 3!")
                    raise ValueError
                if board.matrix[y-1][x-1] != " ":
                    print("This cell is occupied! Choose another one!")
                    raise ValueError
                return y - 1, x - 1
            except ValueError:
                x, y = False, False
                continue


class HardComputerPlayer(MediumComputerPlayer):
    level = "hard"
    borders = [(0, 0), (2, 0), (0, 2), (2, 2)]

    def get_move(self, board) -> tuple:
        blank_options = board.get_empty_cells()
        if len(blank_options) == 9 or (1, 1) in blank_options:
            self.making_move()
            return 1, 1
        free_border = self.free_borders(blank_options)
        if len(blank_options) in (7, 8) and free_border:
            self.making_move()
            return free_border
        return super().get_move(board)

    def free_borders(self, empty_cells):
        for border in self.borders:
            if border in empty_cells:
                return border
players = {
    "user": HumanPlayer,
    "easy": ComputerPlayer,
    "medium": MediumComputerPlayer,
    "hard": HardComputerPlayer
}


class TicTacToeGame:
    x_len = 3
    y_len = 3
    board: TicTacToeBoard
    players: list

    def __init__(self, board: TicTacToeBoard):
        self.board = board

    def get_first_player(self) -> Player:
        count = Counter(self.board.symbols)
        if count["X"] == count["O"]:
            return self.players.pop(0)
        elif count["X"] - 1 == count["O"]:
            return self.players.pop(-1)
        else:
            raise ValueError("Invalid symbols string")

    def start(self):
        while True:
            user_command = input("Input command:").split()
            if not user_command:
                print("Bad parameters!")
                continue
            if user_command[0] not in ("start", "exit"):
                print("Bad parameters!")
                continue
            if user_command[0] == "exit":
                break
            if user_command[0] == "start" and len(user_command) != 3:
                print("Bad parameters!")
                continue

            player_one = players.get(user_command[1])
            player_two = players.get(user_command[2])
            self.players = []
            self.players.append(player_one("X"))
            self.players.append(player_two("O"))
            self.board.render()
            player = self.get_first_player()
            final_result = self.board.game_finished()
            while not final_result:
                y, x = player.get_move(self.board)
                self.board.make_move(player.symbol, y, x)
                self.board.render()
                next_player = self.players.pop()
                self.players.append(player)
                player = next_player
                final_result = self.board.game_finished()
            if isinstance(final_result, str):
                print(f"{final_result} wins")
            else:
                print("Draw")
            # initialize again the board
            self.board = TicTacToeBoard(self.board.symbols)


cells = "_________"
TicTacToeGame(TicTacToeBoard(cells)).start()
