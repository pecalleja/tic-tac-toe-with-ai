from tictactoe import TicTacToeBoard
import pytest


def test_bad_initialization_string():
    with pytest.raises(ValueError):
        TicTacToeBoard("123")

    with pytest.raises(ValueError):
        TicTacToeBoard("__XXOOXO ")


def test_ok_initialization():
    board = TicTacToeBoard("_XXOO_OX_")
    assert hasattr(board, "board")
    assert isinstance(board.board, list)
    y = len(board.board)
    x = len(board.board[0])
    assert x == 3
    assert y == 3

    assert board.board == [
        [" ", "X", "X"],
        ["O", "O", " "],
        ["O", "X", " "],
    ]
